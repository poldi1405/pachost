#!/bin/env bash

source '/etc/makepkg.conf'
source '/etc/pachost.conf'

env PACKAGER="$ph_PACKAGER" PKGDEST="$ph_SERVEPATH" PACMAN="$ph_PACMANALT" \
	makepkg -cCfsr --sign --key "$ph_PACKAGER_SIGNKEY" --asdeps --needed --noconfirm
#	makechrootpkg -c -r "$ph_BUILDDIR" -- -cCfsr -sign --key "$ph_PACKAGER_SIGNKEY" --asdeps --needed --noconfirm

case "$?" in
	"0")
		exit 0
		;;
	"1")
		echo "unknown cause of error terminated makepkg"
		exit 1
		;;
	"2")
		echo "error in configfile terminated makepkg"
		exit 1
		;;
	"3")
		echo "invalid option terminated makepkg"
		exit 1
		;;
	"4")
		echo "error in user-supplied function (PKGBUILD) terminated makepkg"
		exit 1
		;;
	"5")
		echo "unable to create viable package"
		exit 1
		;;
	"6")
		echo "missing file from PKGDIR"
		exit 1
		;;
	"7")
		echo "PKGDIR is missing"
		exit 1
		;;
	"8")
		echo "Failed to install dependencies"
		exit 1
		;;
	"9")
		echo "failed to remove dependencies"
		exit 1
		;;
	"10")
		echo "attempted to run as root"
		exit 1
		;;
	"11")
		echo "user lacks permission to build or install to location"
		exit 1
		;;
	"12")
		echo "error when parsing PKGBUILD"
		exit 1
		;;
	"13")
		echo "a package has already been built"
		exit 1
		;;
	"14")
		echo "the package failed to install"
		exit 1
		;;
	"15")
		echo "necessary programs are missing"
		exit 1
		;;
	"16")
		echo "specified GPG-Key does not exist or failed to sign"
		exit 1
		;;
	*)
		echo "unclear return code from makepkg!"
		exit 1
		;;
esac
